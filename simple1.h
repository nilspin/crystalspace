#ifndef __SIMPLE_H__
#define __SIMPLE_H__

#include<crystalspace.h>

class Simple :public csApplicationFramework, public csBaseEventHandler
{
private:
	csRef<iEngine> engine;
	csRef<iLoader> loader;
	csRef<iGraphics3D> g3d;
	csRef<iKeyboardDriver> kbd;
	csRef<iVirtualClock> vc;
	csRef<iRenderManager> rm;
	csRef<iView> view;
	csRef<FramePrinter> printer;
	bool OnKeyboard(iEvent&);
	iSector* room;
	float rotX, rotY;
	void CreateRoom();
	void CreateSprites();

public:
	Simple();
	~Simple();
	void OnExit();
	bool OnInitialize(int argc, char* argv[]);
	bool Application();
	void Frame();
	bool SetupModules();
	CS_EVENTHANDLER_PHASE_LOGIC("application.simple1")
};

#endif // __SIMPLE_H__