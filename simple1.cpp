#include "simple1.h"
#include<iostream>

CS_IMPLEMENT_APPLICATION

Simple::Simple()
{
	SetApplicationName("Crystalspace.Simple1");
}

Simple::~Simple()
{

}

void Simple::Frame()
{
	csTicks elapsed_time = vc->GetElapsedTicks();
	float speed = (elapsed_time / 1000.0)*(0.03 * 20);
	iCamera *c = view->GetCamera();

	if (kbd->GetKeyState(CSKEY_SHIFT))
	{
		if (kbd->GetKeyState(CSKEY_RIGHT))
			c->Move(CS_VEC_RIGHT * 4 * speed);
		if (kbd->GetKeyState(CSKEY_LEFT))
			c->Move(CS_VEC_LEFT * 4 * speed);
		if (kbd->GetKeyState(CSKEY_UP))
			c->Move(CS_VEC_UP * 4 * speed);
		if (kbd->GetKeyState(CSKEY_DOWN))
			c->Move(CS_VEC_DOWN * 4 * speed);
	}
	else
	{
		if (kbd->GetKeyState(CSKEY_RIGHT))
			rotY += speed;
		if (kbd->GetKeyState(CSKEY_LEFT))
			rotY -= speed;
		if (kbd->GetKeyState(CSKEY_PGUP))
			rotX += speed;
		if (kbd->GetKeyState(CSKEY_PGDN))
			rotX -= speed;
		if (kbd->GetKeyState(CSKEY_UP))
			c->Move(CS_VEC_FORWARD * 4 * speed);
		if (kbd->GetKeyState(CSKEY_DOWN))
			c->Move(CS_VEC_BACKWARD * 4 * speed);
		if (kbd->GetKeyState(CSKEY_F1))
			c->Move(CS_VEC_ROT_LEFT * 3 * speed);
		if (kbd->GetKeyState(CSKEY_F2))
			c->Move(CS_VEC_ROT_RIGHT * 3 * speed);
	}

	csMatrix3 rot = csXRotMatrix3(rotX)*csYRotMatrix3(rotY);
	csOrthoTransform ot(rot, c->GetTransform().GetOrigin());
	c->SetTransform(ot);
	rm->RenderView(view);
}

bool Simple::OnKeyboard(iEvent& ev)
{
	csKeyEventType eventtype = csKeyEventHelper::GetEventType(&ev);
	if (eventtype == csKeyEventTypeDown)
	{
		utf32_char code = csKeyEventHelper::GetCookedCode(&ev);
		if (code == CSKEY_ESC)
		{
			csRef<iEventQueue> q = csQueryRegistry<iEventQueue>(GetObjectRegistry());
			if (q.IsValid()) q->GetEventOutlet()->Broadcast(csevQuit(GetObjectRegistry()));
		}
	}
	return false;
}

bool Simple::OnInitialize(int argc, char* argv[])
{
	if (!csInitializer::RequestPlugins(GetObjectRegistry(),
		CS_REQUEST_VFS,
		CS_REQUEST_OPENGL3D,
		CS_REQUEST_ENGINE,
		CS_REQUEST_FONTSERVER,
		CS_REQUEST_IMAGELOADER,
		CS_REQUEST_LEVELLOADER,
		CS_REQUEST_REPORTER,
		CS_REQUEST_REPORTER,
		CS_REQUEST_END))
		return ReportError("Failed to Initialize Plugins!!!");

	csBaseEventHandler::Initialize(GetObjectRegistry());
	csEventID events[] = {
		csevFrame(GetObjectRegistry()),
		csevKeyboardEvent(GetObjectRegistry()),
		CS_EVENTLIST_END
	};

	if (!RegisterQueue(GetObjectRegistry(), events))
		return ReportError("Failed to Set up event handler!!");

	return true;
}

void Simple::OnExit()
{

}

bool Simple::Application()
{
	if (!OpenApplication(GetObjectRegistry()))
		return ReportError("Error Opening the system!!");
	if (SetupModules())
	{
		Run();
	}
	//CreateSprites();
	return true;
}

void Simple::CreateRoom()
{
	if (!loader->LoadTexture("brick", "/lib/std/stone4.gif"))
		ReportError("Error loading 'brick1_d' texture!");
	iMaterialWrapper* tm = engine->GetMaterialList()->FindByName("brick");

	room = engine->CreateSector("room");

	//First we make a primitive of our geometry
	using namespace CS::Geometry;
	DensityTextureMapper mapper(0.3f);
	TesselatedBox box(csVector3(-5,0,-5), csVector3(5,20,5));
	box.SetLevel(3);
	box.SetMapper(&mapper);
	box.SetFlags(Primitives::CS_PRIMBOX_INSIDE);

	//Now we make a factory and a mesh at once
	csRef<iMeshWrapper> walls = GeneralMeshBuilder::CreateFactoryAndMesh(engine, room, "walls", "walls_factory", &box);
	walls->GetMeshObject()->SetMaterialWrapper(tm);

	csRef<iLight> light;
	iLightList* l1 = room->GetLights();
	light = engine->CreateLight(0, csVector3(-3, 5, 0), 10, csColor(1, 0, 0));
	l1->Add(light);
	light = engine->CreateLight(0, csVector3(3, 5, 0), 10, csColor(0, 1, 0));
	l1->Add(light);
	light = engine->CreateLight(0, csVector3(0,5,-3), 10, csColor(0, 0, 1));
	l1->Add(light);

}

bool Simple::SetupModules()
{
	g3d = csQueryRegistry<iGraphics3D>(GetObjectRegistry()); //i.e g3d = query the registry for "iGraphics3D" type object and pass the address of object registry
	if (!g3d) return ReportError("Failed to locate 3D renderer!");

	engine = csQueryRegistry<iEngine>(GetObjectRegistry());
	if (!engine) return ReportError("Failed to locate game engine!");

	loader = csQueryRegistry<iLoader>(GetObjectRegistry());
	if (!loader) return ReportError("Failed to locate Loader!");

	vc = csQueryRegistry<iVirtualClock>(GetObjectRegistry());
	if (!vc) return ReportError("Failed to locate Virtual Clock!");

	kbd = csQueryRegistry<iKeyboardDriver>(GetObjectRegistry());
	if (!kbd) return ReportError("Failed to locate keyboard driver!");

	rotX = rotY = 0;	//used to store current orientation of camera
	CreateRoom();
	CreateSprites();
	engine->Prepare();
	using namespace CS::Lighting;
	SimpleStaticLighter::ShineLights(room, engine, 4);
	rm = engine->GetRenderManager();

	view.AttachNew(new csView(engine, g3d));
	iGraphics2D* g2d = g3d->GetDriver2D();
	view->SetRectangle(0, 0, g2d->GetWidth(), g2d->GetHeight());

	view->GetCamera()->SetSector(room);
	view->GetCamera()->GetTransform().SetOrigin(csVector3(0, 5, -3));
	printer.AttachNew(new FramePrinter(GetObjectRegistry()));
	return true;
}

void Simple::CreateSprites()
{
	//load a texture for our sprite
	/*iTextureWrapper *txt = loader->LoadTexture("spark", "/lib/std/spark.png");
	if (txt == 0)
	{
		ReportError("Error loading texture!");
	}*/
	//load the sprite template
	csRef<iMeshFactoryWrapper> imeshfact(loader->LoadMeshObjectFactory("/lib/std/sprite2"));
	if (imeshfact == 0)
	{
		ReportError("Error loading mesh object factory!");
	}

	//add a mesh to the engine
	
	csRef<iMeshWrapper> sprite(engine->CreateMeshWrapper(imeshfact, "MySprite", room, csVector3(-3,5,3)));
	
	/* notice above, that the engine creates the mesh. They are not conjured out of thin air or 
	through any other way! */

	csMatrix3 m;

	m.Identity();
	
	sprite->GetMovable()->SetTransform(m);

	sprite->GetMovable()->UpdateMove();
	
	//csRef<iSprite3DState> spstate(scfQueryInterface<iSprite3DState> (sprite->GetMeshObject()));
	//okay, so here we didn't have access to some of the properties of the mesh object(since we are working from engine's perspective).
	//So we create a pointer that queries all the available properties/functions of 3D Sprite type objects, (independent of the engine).
	//Or maybe I'm wrong about the whole thing. I'm sleepy.
	//spstate->SetAction("default");
	
	sprite->SetZBufMode(CS_ZBUF_USE);
	
	sprite->SetRenderPriority(engine->GetObjectRenderPriority());
	//the above two function calls are not compulsory.
}
/* 
-------------------
Main Function
-------------------
*/

int main(int argc, char* argv[])
{
	std::cout << csGetConfigPath();
	return csApplicationRunner<Simple>::Run(argc, argv);
}